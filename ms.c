///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04a - Memory Scanner
///
/// @file ms.c
/// @version 1.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 04a - Memory Scanner - EE 491F - Spr 2021
/// @date   05/02/2021 
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "struct.h"

// Declare global counter variable
int j = 0;

// Declare global struct
maps records[100];

int main(int argc, char *argv[]){

   // Declare file pointer 
   FILE *file;

   // Declare file name
   const char *file_name = "/proc/self/maps"; 
  
   // Declare variable to keep track of current char of fgetc 
   char ch;

   // Declare fgets buffer to store string of each line from /proc/self/maps
   char c[1024]; 
   
   // Declare counter variable
   int i = 0;
   
   // Declare variable for strtok, assists with parsing file contents into struct
   char *token;

   file = fopen(file_name, "r");  

   // Check if file exists
   if ( file == NULL ){
      fprintf(stderr, "ms: Can't open file[%s]", file_name);
      exit(EXIT_FAILURE);
   }

   // Counts total bytes in /proc/self/maps, and number of 'a's in each line of our file
   while ( (ch = fgetc(file)) != EOF ) {
         
         records[i].byte_count++;
   
         if (ch == 'a') {
            records[i].a_count++; 
         }
         
         if (ch == '\n'){
            i++;
         }
   }

   // Set file position to the beginning of the file
   rewind(file); 

   // Copies address & permission fields of /proc/self/maps file into struct records
   while (fgets(c, 1024, file) != NULL){
               
      token = strtok(c, " ");  
      strcpy(records[j].address, token);      
      token = strtok(NULL, " ");
    
      if ( token != NULL) {
         // Case to ignore lines without read permission
         if ( token[0] == '-'){
            continue;
         } 
         strcpy(records[j].permissions, token);  
      }

      // Case to ignore kernel space addresses
      if ( c[0] == 'f' ){
         continue; 
      }
      
      j++;  
   
   }
   
   // Print records content

   for (i = 0; i < j;  i++){
   
      printf("%d: %s %s Number of bytes read [%d] Number of 'a' is [%d]\n", i, records[i].address, records[i].permissions, records[i].byte_count, records[i].a_count); 
   
   }
   
   return EXIT_SUCCESS; 
}
