CC     = gcc
CFLAGS = -g -Wall

TARGET = ms

all: $(TARGET)

ms: ms.c 
	$(CC) $(CFLAGS) -o $(TARGET) ms.c

test: 
	./ms
clean:
	rm $(TARGET)

