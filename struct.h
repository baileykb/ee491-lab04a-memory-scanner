///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 491 - Software Reverse Engineering
/// Lab 04a - Memory Scanner
///
/// @file struct.h
/// @version 1.0
///
/// @author Bailey Borengasser <baileykb@hawaii.edu>
/// @brief  Lab 04a - Memory Scanner - EE 491F - Spr 2021
/// @date   08/02/2021
///////////////////////////////////////////////////////////////////////////////

#define MAX 26

// Struct used for parsing contents of /proc/self/maps file

typedef struct maps_struct {

   char address[MAX];
   char permissions[6];
   int a_count;
   int byte_count; 

   } maps; 
